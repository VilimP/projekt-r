import requests
import hashlib
from bs4 import BeautifulSoup


links_of_libraries = open("linksOfLibraries.txt", "r")
result = open("result.txt", "w")

line = links_of_libraries.readline()
while (line):
    line = line.strip().split(",")
    main_ulr = line[0]
    sample_url = line[1]
    print("main_ulr " + main_ulr)
    print("sample_url " + sample_url)
    print()

    page = requests.get(main_ulr)

    soup = BeautifulSoup(page.content, 'html.parser')

    links = []

    all_a = soup.find_all('a', href=True)
    for a in all_a:
        link = a['href']
        if sample_url in link:
            if link.endswith(".js"):
                links.append(link)

    for link in links:
        print(link)
        print()
        src_code = requests.get(link)

        name = link[link.index("libs")+5:]
        name = name.replace("/", "_")

        jsfile = open("jslibraries-src/"+name, "wb")
        #jsfile.write(str(src_code.content).encode('utf-8'))
        jsfile.write(src_code.content)
        jsfile.close()

        #hash_value = hashlib.sha256(str(src_code.content).encode('utf-8'))
        hash_value = hashlib.sha256(str(src_code.content.decode('utf-8')).encode('utf-8')) #decode zbog pretvaranja iz niza okteta u niz znakova (utjece na hash)
        
        digest = hash_value.hexdigest()

        result.write(name + "," + digest + "," + link + "\n")

    f = open("tmplinks.txt", "a")
    f.write(str(links))
    f.close()

    line = links_of_libraries.readline()

links_of_libraries.close()
