import json
import re

import pymongo
from pymongo import MongoClient

cluster = MongoClient(
    "mongodb+srv://korisnik:mongodb123@cluster0.t7o92.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
db = cluster["MyDataBase"]
collection = db["libraries"]

file = open("../get_and_hash_script/result-full.txt", "r")
libraries = []

id_counter = 1
for line in file:
    (name_version, hashvalue, url) = (line.split(","))

    splited_name_version = name_version.split("_")

    name = splited_name_version[0]
    version = splited_name_version[1]

    # print(name)
    # print(version)

    formatted_line = {"_id": id_counter}
    id_counter += 1

    formatted_line["name"] = name
    formatted_line["version"] = version
    formatted_line["hashvalue"] = hashvalue

    libraries.append(formatted_line)

# print(libraries)
collection.insert_many(libraries)