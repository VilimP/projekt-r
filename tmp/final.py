import requests
import hashlib
from bs4 import BeautifulSoup
import pymongo
import time

client = pymongo.MongoClient("mongodb://localhost:27017/")

#mongo database
db = client["js_libraries_v0"]

#collection of js libraries
col_jslibraries = db["js_libraries"]

#cdnpkg.com ulrs for downloading library source codes
col_download_urls = db["js_libraries_download_urls"]

#Retire.js repository
col_retirejs = db["retirejs_repo"]

#sample of a link to look for when scraping
download_link_sample = "https://cdnjs.cloudflare.com/ajax/libs/"


def fill_db():
    urls = col_download_urls.find({})
    
    for url in urls:

        url = url["url"]

        page = requests.get(url)

        soup = BeautifulSoup(page.content, 'html.parser')

        scraped_download_links = []

        all_a_tags = soup.find_all('a', href=True)

        for a in all_a_tags:
            download_link = a['href']
            
            if download_link_sample in download_link:
                if download_link.endswith(".js"):
                    scraped_download_links.append(download_link)

        for download_link in scraped_download_links:

            if col_jslibraries.count_documents({"url": download_link}) > 0:
                continue

            jslibrary_source_code = requests.get(download_link).content.decode('utf-8')
            
            extracted_name = download_link[len(download_link_sample):]
            
            name, version, library_type = extracted_name.split("/")

            if ("min" in library_type and "slim" in library_type):
                library_type = "slim-min"
            elif("min" in library_type and "slim" not in library_type):
                library_type = "min"
            elif("min" not in library_type and "slim" in library_type):
                library_type = "slim"
            else:
                library_type = "uncompressed"

            hash_value = hashlib.sha256(str(jslibrary_source_code).encode('utf-8'))

            hash_digest = hash_value.hexdigest()

            timestamp = time.time()

            vuln_list = find_vulnerabilities({"name": name, "version": version})

            formatted_doc = {"name": name,
                            "version": version,
                            "type": library_type,
                            "url": download_link,
                            "hash_value": hash_digest,
                            "timestamp": timestamp,
                            "vulnerabilities": vuln_list,
                            "source_code": jslibrary_source_code}
                

            col_jslibraries.insert_one(formatted_doc)



def find_vulnerabilities(library):



def fill_retirejs():






























