from pymongo import MongoClient
import json
import time

# isprobaval sam nekaj


cluster = MongoClient(
    "mongodb+srv://korisnik:mongodb123@cluster0.t7o92.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
db = cluster["MyDataBase"]
collection_retire = db["retireJs"]
collection_lib = db["libraries"]


def fill_db():
    file = open("../get_and_hash_script/result-full-decoded2.txt", "r")
    libraries = []
    results = collection_retire.find({})

    for line in file:
        (name_version, hashvalue, url) = (line.split(","))
        url = url[:len(url) - 1]

        splited_name_version = name_version.split("_")

        name = splited_name_version[0]
        version = splited_name_version[1]

        timestamp = time.time()

        formatted_line = {"name": name,
                          "version": version,
                          "url": url,
                          "hashvalue": hashvalue,
                          "timestamp": timestamp,
                          "vulnerabilities": []}
        vuln_lista = find_vulnerability(formatted_line, results)

        if len(vuln_lista) != 0:
            formatted_line.update({"vulnerabilities": vuln_lista})

        libraries.append(formatted_line)

    collection_lib.insert_many(libraries)


def find_vulnerability(library, results):
    results = collection_retire.find({})
    found = False
    vuln_lista = []

    for result in results:
        for name in result:
            if name == "_id":
                continue
            vuln_dict = result[name]
            if "vulnerabilities" in vuln_dict:
                vulnerabilities = vuln_dict["vulnerabilities"]
                below = ""
                atOrAbove = ""

                for vuln in vulnerabilities:
                    if "below" in vuln:
                        below = vuln["below"]
                    if "atOrAbove" in vuln:
                        atOrAbove = vuln["atOrAbove"]

                    if library["name"] == name:
                        if library["version"] < below and library["version"] >= atOrAbove:
                            # print(f'{library["name"]} - {library["version"]} is vulnerable -> (below={below}, atOrAbove={atOrAbove}) => vulnerability={vuln["identifiers"]}')
                            found = True
                            vuln_lista.append(vuln["identifiers"])

    # if not found:
    #     print(f'{library["name"]} - {library["version"]} is NOT vulnerable')

    found = False

    return vuln_lista


# main

fill_db()
