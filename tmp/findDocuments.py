results = collection.find({})

for result in results:
    for el in result:
        if el == "_id":
            continue
        vuln_dict = result[el]
        if "vulnerabilities" in vuln_dict:
            vulnerabilities = vuln_dict["vulnerabilities"]
            for vuln in vulnerabilities:
                #print("vuln="+str(vuln))
                if "below" in vuln:
                    below = vuln["below"]
                    print("below="+below)
                if "atOrAbove" in vuln:
                    atOrAbove = vuln["atOrAbove"]
                    print("atOrAbove="+atOrAbove)
                print(f"Vulnerability {below} - {atOrAbove}")