import pymongo
from pymongo import MongoClient

cluster = MongoClient(
    "mongodb+srv://korisnik:mongodb123@cluster0.t7o92.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
db = cluster["test"]
collection = db["test"]

post1 = {"_id": 5, "name": "Joe"}
post2 = {"_id": 6, "name": "Bill"}

# collection.insert_one(post1)
# collection.insert_many([post1, post2])

# resulte je Cursor object
# results = collection.find({"name":"Bill"})
#
# for result in results:
#     print(result["_id"])

# results = collection.find_one({"name": "Joe"})
# print(results)

# vraca sve (dokumente)
# results = collection.find({})
#
# for x in results:
#     print(x)

# obrisati samo jedan dokument
# results = collection.delete_one({"_id": 0})
# obrisati puno(u ovom slucaju sve)
# results = collection.delete_many({})

# results = collection.update_one({"_id": 5}, {"$set": {"hello": 5}})
# results = collection.update_many({"_id": 5}, {"$set": {"hello": 5}})

post_count = collection.count_documents({})
print(post_count)