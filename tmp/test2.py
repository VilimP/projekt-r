import hashlib
import fileinput
import requests
from bs4 import BeautifulSoup


def linkoviSaStranice():
    page = requests.get("https://releases.jquery.com/jquery/")
    # print("linkovi sa stranice")
    # print(page.content)

    soup = BeautifulSoup(page.content, 'html.parser')

    # print(soup.prettify())

    links = []

    all_a = soup.find_all('a', href=True)

    for a in all_a:
        link = a['href']
        if "https://code.jquery.com/jquery-" in link:
            links.append(link)

    out_file = open("links.txt", "w")
    for link in links:
        out_file.write(link + "\n")
    out_file.close()

    return links


def hashiranjeIspremanje(links):
    out_file = open("result.txt", "w")

    for i in range(0, len(links)):
        out_file.write("{")
        out_file.write('"ulr": "' + links[i] + '", ')
        source_code = requests.get(links[i])
        hash_value = hashlib.sha256(str(source_code.content).encode('utf-8'))
        out_file.write('"hashvalue": "' + hash_value.hexdigest())

        out_file.write('"},\n')

    out_file.close()


##main

links = linkoviSaStranice()

hashiranjeIspremanje(links)

print("finished")
