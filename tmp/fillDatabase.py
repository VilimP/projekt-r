from pymongo import MongoClient
import time
import json
import hashlib
import requests
from bs4 import BeautifulSoup

cluster = MongoClient(
    "mongodb+srv://korisnik:mongodb123@cluster0.t7o92.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
db = cluster["MyDataBase"]
collection_lib = db["libraries"]
collection_retire = db["retireJs"]


def fill_db():
    file = open("../get_and_hash_script/result-full-decoded.txt", "r")
    libraries = []
    results_retire = collection_retire.find({})

    for line in file:
        (name_version, hashvalue) = (line.split(","))
        hashvalue = hashvalue[:len(hashvalue) - 1]
        print(hashvalue)

        splited_name_version = name_version.split("_")

        name = splited_name_version[0]
        version = splited_name_version[1]

        timestamp = time.time()

        formatted_line = {"name": name, "version": version, "hashvalue": hashvalue, "timestamp": timestamp}

        libraries.append(formatted_line)

    # print(libraries)
    collection_lib.insert_many(libraries)


def check_database(input_lib):
    if collection_lib.count_documents({"name": input_lib[0], "version": input_lib[1]}) != 0:
        results = collection_lib.find({"name": input_lib[0], "version": input_lib[1]})
        for result in results:
            if input_lib[2] == result["hashvalue"]:
                print("Biblioteka nađena!")
            else:
                print("Hash se ne poklapa!")
    else:
        print("Biblioteka se ne nalazi u bazi!")


def clear_database():
    permission = input("Želite li obrisati cijelu bazu? (Y,N)")
    if permission == "Y":
        collection_lib.delete_many({})


def print_all():
    print(collection_lib.count_documents({}))
    results = collection_lib.find({})
    for result in results:
        print(result)


def linkoviSaStranice():
    page = requests.get("https://releases.jquery.com/jquery/")
    # print("linkovi sa stranice")
    # print(page.content)

    soup = BeautifulSoup(page.content, 'html.parser')

    # print(soup.prettify())

    links = []

    all_a = soup.find_all('a', href=True)

    for a in all_a:
        link = a['href']
        if "https://code.jquery.com/jquery-" in link:
            links.append(link)

    out_file = open("links.txt", "w")
    for link in links:
        out_file.write(link + "\n")
    out_file.close()

    return links


def hashiranjeIspremanje(links):
    out_file = open("result.txt", "w")

    for i in range(0, len(links)):
        out_file.write("{")
        out_file.write('"ulr": "' + links[i] + '", ')
        source_code = requests.get(links[i])
        hash_value = hashlib.sha256(str(source_code.content).encode('utf-8'))
        out_file.write('"hashvalue": "' + hash_value.hexdigest())

        out_file.write('"},\n')

    out_file.close()


# main