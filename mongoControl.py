from pymongo import MongoClient
import time
import json
import hashlib
import requests
from bs4 import BeautifulSoup

# Connect to our MongoDB database
cluster = MongoClient(
    "mongodb+srv://korisnik:mongodb123@cluster0.t7o92.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
db = cluster["MyDataBase"]
collection_retire = db["retireJs"]
collection_lib = db["libraries"]


def fill_db():
    file = open("get_and_hash_script/result-full-decoded2.txt", "r")
    libraries = []                          # lista n-torki koje treba ubaciti u bazu
    results = collection_retire.find({})    # sadržaj kolekcije RetireJS iz naše baze


    for line in file:
        (name_version, hashvalue, url) = (line.split(","))  # iz linije izvuci podatke
        url = url[:len(url) - 1]    # zbog oznake \n

        splitted_name_version = name_version.split("_")

        name = splitted_name_version[0]
        version = splitted_name_version[1]

        timestamp = time.time()

        # struktura pojedinog dokumenta u nasoj bazi
        formatted_line = {"name": name,
                          "version": version,
                          "url": url,
                          "hashvalue": hashvalue,
                          "timestamp": timestamp,
                          "vulnerabilities": []}

        vuln_list = find_vulnerabilities(formatted_line, results)

        if len(vuln_list) != 0:
            formatted_line.update({"vulnerabilities": vuln_list})

        libraries.append(formatted_line)

    collection_lib.insert_many(libraries)


def find_vulnerabilities(library, results):
    results = collection_retire.find({})
    found = False
    vuln_lista = []

    for result in results:
        for name in result:
            if name == "_id":
                continue
            vuln_dict = result[name]
            if "vulnerabilities" in vuln_dict:
                vulnerabilities = vuln_dict["vulnerabilities"]
                below = ""
                atOrAbove = ""

                for vuln in vulnerabilities:
                    if "below" in vuln:
                        below = vuln["below"]
                    if "atOrAbove" in vuln:
                        atOrAbove = vuln["atOrAbove"]

                    if library["name"] == name:
                        if library["version"] < below and library["version"] >= atOrAbove:
                            # print(f'{library["name"]} - {library["version"]} is vulnerable -> (below={below}, atOrAbove={atOrAbove}) => vulnerability={vuln["identifiers"]}')
                            found = True
                            vuln_lista.append(vuln["identifiers"])

    # if not found:
    #     print(f'{library["name"]} - {library["version"]} is NOT vulnerable')

    found = False

    return vuln_lista


# Promijeniti uvijete po kojima ispituje postojanje biblioteke u naštoj bazi
def database_contains(input_lib_hash):
    result = collection_lib.find({})

    for res in result:
        if input_lib_hash == res["hashvalue"]:
            print("Found!!")
            print(res)


def clear_database():
    permission = input("Želite li obrisati cijelu bazu? (Y,N)")
    if permission.upper() == "Y":
        collection_lib.delete_many({})


def print_all():
    print(collection_lib.count_documents({}))
    results = collection_lib.find({})
    for result in results:
        print(result)


# Main




database_contains("c942686010e285633d77a24341c43850ccd6162fcc7e8281ae8a70c2921a9af5")