import requests
import hashlib
from bs4 import BeautifulSoup
import pymongo
import time
import json

##
import os.path


#pymongo client
client = pymongo.MongoClient("mongodb://localhost:27018/")

#mongo database
db = client["js_libraries_v3"]

#collection of js libraries
col_js_libraries = db["js_libraries"]

#cdnpkg.com ulrs for downloading library source codes
col_download_urls = db["js_libraries_download_urls"]

#Retire.js repository
col_retirejs = db["retirejs_repo"]



def fill_js_libraries_db():

    #sample of a link to look for when scraping
    download_link_sample = "https://cdnjs.cloudflare.com/ajax/libs/"

    urls = col_download_urls.find({}) #.limit(1)
    
    for url in urls:

        url = url["url"]

        page = requests.get(url)

        soup = BeautifulSoup(page.content, 'html.parser')

        scraped_download_links = []

        all_a_tags = soup.find_all('a', href=True)

        for a in all_a_tags:
            download_link = a['href']
            
            if download_link_sample in download_link:
                if download_link.endswith(".js"):
                    scraped_download_links.append(download_link)

        for download_link in scraped_download_links:

            #skip if already exists in database
            if col_js_libraries.count_documents({"url": download_link}) > 0:
                continue

            jslibrary_source_code = requests.get(download_link).content.decode('utf-8')

            ##################### lokalne datoteke umjesto preuzimanja s interneta #########################
            # fn = download_link[len(download_link_sample):]
            # fn = fn.replace("/","_")
            # jslibrary_source_code = ""
            # if os.path.exists("/media/sf_SharedFolder/sf/oldsf/jssourcecodes/" + fn):
            #     f = open("/media/sf_SharedFolder/sf/oldsf/jssourcecodes/" + fn, "r")
            #     jslibrary_source_code = f.read()
            #     f.close()
            # else:
            #     print("Downloading " + download_link)
            #     jslibrary_source_code = requests.get(download_link).content.decode('utf-8')
            ##############################################

            extracted_name = download_link[len(download_link_sample):]
            
            extracted_name_list= extracted_name.split("/")
            name = extracted_name_list[0]
            version = extracted_name_list[1]
            file_name = extracted_name_list[-1]

            library_type = "uncompressed"
            if ("min" in file_name and "slim" in file_name):
                library_type = "slim-min"
            elif("min" in file_name and "slim" not in file_name):
                library_type = "min"
            elif("min" not in file_name and "slim" in file_name):
                library_type = "slim"

            release_phase = "ga" #General availability
            if "-" in version:
                dashindex = version.index("-")
                release_phase = version[dashindex+1:]
                version = version[0:dashindex]

            hash_value = hashlib.sha256(str(jslibrary_source_code).encode('utf-8'))

            hash_digest = hash_value.hexdigest()

            timestamp = time.time()

            formatted_doc = {"name": name,
                            "file_name": file_name,
                            "version": version,
                            "release_phase": release_phase,
                            "type": library_type,
                            "url": download_link,
                            "hash": hash_digest,
                            "timestamp": timestamp,
                            "vulnerabilities": [],
                            "source_code": jslibrary_source_code}

            vuln_list = find_vulnerabilities(formatted_doc)

            if len(vuln_list) != 0:
               formatted_doc.update({"vulnerabilities": vuln_list})

            col_js_libraries.insert_one(formatted_doc)
            
            print("[+] Added " + download_link + " library to database")
    print("[+] Updated js_libraries")


def find_vulnerabilities(library):

    docs = col_retirejs.find({"name": library["name"]})

    vuln_list = []

    for doc in docs:
        data = doc["data"]
        vulnerabilities = data["vulnerabilities"]

        below = ""
        atOrAbove = ""

        for vuln in vulnerabilities:
            if "below" in vuln:
                below = vuln["below"]
            if "atOrAbove" in vuln:
                atOrAbove = vuln["atOrAbove"]

            if library["version"] < below and library["version"] >= atOrAbove:
                vuln_list.append(vuln["identifiers"])

    return vuln_list


def fill_retirejs():
 
    link = "https://raw.githubusercontent.com/RetireJS/retire.js/master/repository/jsrepository.json"

    page = requests.get(link).content.decode('utf-8')

    #page = open("jsrepository.json", "r").read()

    pagedict = json.loads(page)

    for key in pagedict.keys():

        flag = 0
        if col_retirejs.count_documents({"name": key}) > 0:
            docs = col_retirejs.find({"name": key})
            for doc in docs:
                if (doc["data"] == pagedict.get(key)):
                    flag = 1
        if flag == 1:
            continue

        col_retirejs.update_one({"name": key}, 
            {"$set": {"name": key,"data": pagedict.get(key)}},
            upsert=True)
        print("[+] Updated " + key)


def update_vulnerabilities():
    docs = col_js_libraries.find({})

    for doc in docs:
        vuln_list = find_vulnerabilities(doc)
        if vuln_list == doc["vulnerabilities"]:
            continue
        col_js_libraries.update_one({"_id": doc["_id"]},
                                    {"$set": {"vulnerabilities": vuln_list}})

        print("Updated vulnerabilities " + doc["name"] + doc["version"])


def fill_js_libraries_download_urls():
    f = open("js_libraries_download_urls.txt", "r")
    
    line = f.readline().strip()

    while(line):
        
        if col_download_urls.count_documents({"url": line}) > 0:
            line = f.readline().strip()
            continue

        col_download_urls.insert_one({"url": line})

        line = f.readline().strip()

    f.close()
    print("[+] Updated download urls")


def db_statistics():
    x = col_download_urls.count_documents({})
    print("Collection js_libraries_download_urls contains " + str(x) + " documents")
    
    x = col_retirejs.count_documents({})
    print("Collection retirejs_repo contains " + str(x) + " documents")

    names = []
    docs = col_js_libraries.find({}, {"name": 1})
    for doc in docs:
        if doc["name"] not in names:
            names.append(doc["name"])
    
    x = col_js_libraries.count_documents({})
    print("Collection js_libraries contains " + str(x) + " documents for " + str(len(names)) + " js libraries")


def detect():
    t1 = time.time()
    
    #websecradar db

    #credentials
    username = ""
    password = ""

    #pymongo client
    client_wsc = pymongo.MongoClient("mongodb://" + username +":" + password + "@localhost:27017/")
    
    #database
    db_wsc = client_wsc["websecradar"]

    #collection
    col_crawled_data_pages = db_wsc["crawled_data_pages_v0"]

    #docs = col_crawled_data_pages.find({})
    limit = 50000
    docs = col_crawled_data_pages.find({}, {"hash": 1}).limit(limit)

    count = 0

    f = open("tmp_result.txt", "w")

    for doc in docs:
        resdoc = col_js_libraries.find({"hash": doc["hash"]})

        for i in resdoc:
            count += 1

            vuln = " is NOT vulnerable"
            if len(i["vulnerabilities"]) > 0:
                vuln = " is VULNERABLE"

            f.write("[" + str(count) +"]" + " Detected: " + i["name"] + " " + i["version"] + " " + i["type"] + vuln + "\n")
            #print("[" + str(count) +"]" + " Detected: " + i["name"] + " " + i["version"] + " " + i["type"] + vuln + "\n")

    f.write("\nDetected " + str(count) + " out of " + str(limit) + " documents\n") 
    f.write("Time elapsed: " + str(time.time()-t1) + "s")
    #print("\nDetected " + str(count) + " out of " + str(limit) + " documents\n") 
    #print("Time elapsed: " + str(time.time()-t1) + "s")

    f.close()



#main

# fill_retirejs()

# fill_js_libraries_download_urls()

# fill_js_libraries_db()

# update_vulnerabilities()

# db_statistics()

# detect()


